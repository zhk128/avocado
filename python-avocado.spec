%global srcname avocado

# Settings used for build from snapshots.
%global gittar          %{srcname}-%{version}.tar.gz

# Selftests are provided but may need to be skipped because many of
# the functional tests are time and resource sensitive and can
# cause race conditions and random build failures. They are
# enabled by default.
%global with_python3_aexpect 1

# The Python dependencies are already tracked by the python2
# or python3 "Requires".  This filters out the python binaries
# from the RPM automatic requires/provides scanner.
%global __requires_exclude ^/usr/bin/python[23]$

Summary:       Framework with tools and libraries for Automated Testing
Name:          python-%{srcname}
Version:       77.0
Release:       0%{?gitrel}%{?dist}
License:       GPLv2
Group:         Development/Tools
URL:           http://avocado-framework.github.io/
Source0:       https://github.com/avocado-framework/%{srcname}/archive/%{version}.tar.gz#/%{gittar}
BuildArch:     noarch
BuildRequires: procps-ng
BuildRequires: kmod
%if %{with_python3_aexpect}
BuildRequires: python3-aexpect
%endif

BuildRequires: python3-jinja2
BuildRequires: python3-devel
BuildRequires: python3-docutils
BuildRequires: python3-lxml
BuildRequires: python3-psutil
BuildRequires: python3-setuptools

%description
Avocado is a set of tools and libraries (what people call
these days a framework) to perform automated testing.

%package -n python3-%{srcname}
Summary:       %{summary}
Requires:      python3-%{srcname}-common == %{version}
Requires:      gdb
Requires:      gdb-gdbserver
Requires:      procps-ng
Requires:      python3
Requires:      python3-setuptools

%description -n python3-%{srcname}
Avocado is a set of tools and libraries (what people call
these days a framework) to perform automated testing.

%prep
%setup -q -n %{srcname}-%{version}

%build
%py3_build
pushd optional_plugins/html
%py3_build
popd
pushd optional_plugins/runner_remote
popd
pushd optional_plugins/runner_vm
popd
pushd optional_plugins/runner_docker
popd
pushd optional_plugins/resultsdb
%py3_build
popd
pushd optional_plugins/varianter_yaml_to_mux
%py3_build
popd
pushd optional_plugins/loader_yaml
%py3_build
popd
pushd optional_plugins/golang
%py3_build
popd
pushd optional_plugins/varianter_pict
%py3_build
popd
pushd optional_plugins/varianter_cit
%py3_build
popd
pushd optional_plugins/result_upload
%py3_build
popd
pushd optional_plugins/glib
%py3_build
popd
%{__make} man

%install
%py3_install
%{__cp} -r scripts %{buildroot}%{python3_sitelib}/avocado_framework-77.0-py3.8.egg-info
%{__mv} %{buildroot}%{python3_sitelib}/avocado/etc %{buildroot}
%{__sed} -e 's/ etc\/avocado\/sysinfo\// \/etc\/avocado\/sysinfo\//' -i %{buildroot}/etc/avocado/avocado.conf
pushd optional_plugins/html
%py3_install
popd
pushd optional_plugins/runner_remote
popd
pushd optional_plugins/runner_vm
popd
pushd optional_plugins/runner_docker
popd
pushd optional_plugins/resultsdb
%py3_install
popd
pushd optional_plugins/varianter_yaml_to_mux
%py3_install
popd
pushd optional_plugins/loader_yaml
%py3_install
popd
pushd optional_plugins/golang
%py3_install
popd
pushd optional_plugins/varianter_pict
%py3_install
popd
pushd optional_plugins/varianter_cit
%py3_install
popd
pushd optional_plugins/result_upload
%py3_install
popd
pushd optional_plugins/glib
%py3_install
popd
%{__mkdir} -p %{buildroot}%{_mandir}/man1
%{__install} -m 0644 man/avocado.1 %{buildroot}%{_mandir}/man1/avocado.1
%{__install} -d -m 0755 %{buildroot}%{_sharedstatedir}/avocado/data
%{__install} -d -m 0755 %{buildroot}%{_docdir}/avocado
%{__cp} -r examples/gdb-prerun-scripts %{buildroot}%{_docdir}/avocado
%{__cp} -r examples/plugins %{buildroot}%{_docdir}/avocado
%{__cp} -r examples/tests %{buildroot}%{_docdir}/avocado
%{__cp} -r examples/wrappers %{buildroot}%{_docdir}/avocado
%{__cp} -r examples/yaml_to_mux %{buildroot}%{_docdir}/avocado
%{__cp} -r examples/yaml_to_mux_loader %{buildroot}%{_docdir}/avocado
%{__cp} -r examples/varianter_pict %{buildroot}%{_docdir}/avocado
%{__cp} -r examples/varianter_cit %{buildroot}%{_docdir}/avocado
find %{buildroot}%{_docdir}/avocado -type f -name '*.py' -exec %{__chmod} -c -x {} ';'
%{__mkdir} -p %{buildroot}%{_libexecdir}/avocado
%{__mv} %{buildroot}%{python3_sitelib}/avocado/libexec/* %{buildroot}%{_libexecdir}/avocado

%files -n python3-%{srcname}
%defattr(-,root,root,-)
%doc README.rst LICENSE
%{_bindir}/avocado
%{_bindir}/avocado-runner
%{_bindir}/avocado-runner-noop
%{_bindir}/avocado-runner-exec
%{_bindir}/avocado-runner-exec-test
%{_bindir}/avocado-runner-python-unittest
%{_bindir}/avocado-runner-avocado-instrumented
%{_bindir}/avocado-runner-tap
%{_bindir}/avocado-software-manager
%{python3_sitelib}/avocado*
%exclude %{python3_sitelib}/avocado_result_html*
%exclude %{python3_sitelib}/avocado_runner_remote*
%exclude %{python3_sitelib}/avocado_runner_vm*
%exclude %{python3_sitelib}/avocado_runner_docker*
%exclude %{python3_sitelib}/avocado_resultsdb*
%exclude %{python3_sitelib}/avocado_loader_yaml*
%exclude %{python3_sitelib}/avocado_golang*
%exclude %{python3_sitelib}/avocado_varianter_yaml_to_mux*
%exclude %{python3_sitelib}/avocado_varianter_pict*
%exclude %{python3_sitelib}/avocado_varianter_cit*
%exclude %{python3_sitelib}/avocado_result_upload*
%exclude %{python3_sitelib}/avocado_glib*
%exclude %{python3_sitelib}/avocado_framework_plugin_result_html*
%exclude %{python3_sitelib}/avocado_framework_plugin_runner_remote*
%exclude %{python3_sitelib}/avocado_framework_plugin_runner_vm*
%exclude %{python3_sitelib}/avocado_framework_plugin_runner_docker*
%exclude %{python3_sitelib}/avocado_framework_plugin_resultsdb*
%exclude %{python3_sitelib}/avocado_framework_plugin_varianter_yaml_to_mux*
%exclude %{python3_sitelib}/avocado_framework_plugin_varianter_pict*
%exclude %{python3_sitelib}/avocado_framework_plugin_varianter_cit*
%exclude %{python3_sitelib}/avocado_framework_plugin_loader_yaml*
%exclude %{python3_sitelib}/avocado_framework_plugin_golang*
%exclude %{python3_sitelib}/avocado_framework_plugin_result_upload*
%exclude %{python3_sitelib}/avocado_framework_plugin_glib*
%exclude %{python3_sitelib}/tests*

%package -n python3-%{srcname}-common
Summary:       Avocado common files

%description -n python3-%{srcname}-common
Common files (such as configuration) for the Avocado Testing Framework.

%files -n python3-%{srcname}-common
%{_mandir}/man1/avocado.1.gz
%dir %{_sysconfdir}/avocado
%dir %{_sysconfdir}/avocado/conf.d
%dir %{_sysconfdir}/avocado/sysinfo
%dir %{_sysconfdir}/avocado/scripts
%dir %{_sysconfdir}/avocado/scripts/job
%dir %{_sysconfdir}/avocado/scripts/job/pre.d
%dir %{_sysconfdir}/avocado/scripts/job/post.d
%dir %{_sharedstatedir}/avocado
%config(noreplace)%{_sysconfdir}/avocado/avocado.conf
%config(noreplace)%{_sysconfdir}/avocado/conf.d/README
%config(noreplace)%{_sysconfdir}/avocado/conf.d/gdb.conf
%config(noreplace)%{_sysconfdir}/avocado/conf.d/jobscripts.conf
%config(noreplace)%{_sysconfdir}/avocado/sysinfo/commands
%config(noreplace)%{_sysconfdir}/avocado/sysinfo/files
%config(noreplace)%{_sysconfdir}/avocado/sysinfo/profilers
%config(noreplace)%{_sysconfdir}/avocado/scripts/job/pre.d/README
%config(noreplace)%{_sysconfdir}/avocado/scripts/job/post.d/README

%package -n python3-%{srcname}-plugins-output-html
Summary:       Avocado HTML report plugin
Requires:      python3-%{srcname} == %{version}, python3-jinja2

%description -n python3-%{srcname}-plugins-output-html
Adds to avocado the ability to generate an HTML report at every job results
directory. It also gives the user the ability to write a report on an
arbitrary filesystem location.

%files -n python3-%{srcname}-plugins-output-html
%{python3_sitelib}/avocado_result_html*
%{python3_sitelib}/avocado_framework_plugin_result_html*

%package -n python3-%{srcname}-plugins-resultsdb
Summary:       Avocado plugin to propagate job results to ResultsDB
Requires:      python3-%{srcname} == %{version}
Requires:      python3-resultsdb_api

%description -n python3-%{srcname}-plugins-resultsdb
Allows Avocado to send job results directly to a ResultsDB
server.

%files -n python3-%{srcname}-plugins-resultsdb
%{python3_sitelib}/avocado_resultsdb*
%{python3_sitelib}/avocado_framework_plugin_resultsdb*
%config(noreplace)%{_sysconfdir}/avocado/conf.d/resultsdb.conf

%package -n python3-%{srcname}-plugins-varianter-yaml-to-mux
Summary:       Avocado plugin to generate variants out of yaml files
Requires:      python3-%{srcname} == %{version}
Requires:      python3-yaml

%description -n python3-%{srcname}-plugins-varianter-yaml-to-mux
Can be used to produce multiple test variants with test parameters
defined in a yaml file(s).

%files -n python3-%{srcname}-plugins-varianter-yaml-to-mux
%{python3_sitelib}/avocado_varianter_yaml_to_mux*
%{python3_sitelib}/avocado_framework_plugin_varianter_yaml_to_mux*

%package -n python3-%{srcname}-plugins-loader-yaml
Summary:       Avocado Plugin that loads tests from YAML files
Requires:      python3-%{srcname}-plugins-varianter-yaml-to-mux == %{version}

%description -n python3-%{srcname}-plugins-loader-yaml
Can be used to produce a test suite from definitions in a YAML file,
similar to the one used in the yaml_to_mux varianter plugin.

%files -n python3-%{srcname}-plugins-loader-yaml
%{python3_sitelib}/avocado_loader_yaml*
%{python3_sitelib}/avocado_framework_plugin_loader_yaml*

%package -n python3-%{srcname}-plugins-golang
Summary:       Avocado Plugin for Execution of golang tests
Requires:      python3-%{srcname} == %{version}
Requires:      golang

%description -n python3-%{srcname}-plugins-golang
Allows Avocado to list golang tests, and if golang is installed,
also run them.

%files -n python3-%{srcname}-plugins-golang
%{python3_sitelib}/avocado_golang*
%{python3_sitelib}/avocado_framework_plugin_golang*

%package -n python3-%{srcname}-plugins-varianter-pict
Summary:       Varianter with combinatorial capabilities by PICT
Requires:      python3-%{srcname} == %{version}

%description -n python3-%{srcname}-plugins-varianter-pict
This plugin uses a third-party tool to provide variants created by
Pair-Wise algorithms, also known as Combinatorial Independent Testing.

%files -n python3-%{srcname}-plugins-varianter-pict
%{python3_sitelib}/avocado_varianter_pict*
%{python3_sitelib}/avocado_framework_plugin_varianter_pict*

%package -n python3-%{srcname}-plugins-varianter-cit
Summary:       Varianter with Combinatorial Independent Testing capabilities
Requires:      python3-%{srcname} == %{version}

%description -n python3-%{srcname}-plugins-varianter-cit
A varianter plugin that generates variants using Combinatorial
Independent Testing (AKA Pair-Wise) algorithm developed in
collaboration with CVUT Prague.

%files -n python3-%{srcname}-plugins-varianter-cit
%{python3_sitelib}/avocado_varianter_cit*
%{python3_sitelib}/avocado_framework_plugin_varianter_cit*

%package -n python3-%{srcname}-plugins-result-upload
Summary:       Avocado Plugin to propagate Job results to a remote host
Requires:      python3-%{srcname} == %{version}

%description -n python3-%{srcname}-plugins-result-upload
This optional plugin is intended to upload the Avocado Job results to
a dedicated sever.

%files -n python3-%{srcname}-plugins-result-upload
%{python3_sitelib}/avocado_result_upload*
%{python3_sitelib}/avocado_framework_plugin_result_upload*
%config(noreplace)%{_sysconfdir}/avocado/conf.d/result_upload.conf

%package -n python3-%{srcname}-plugins-glib
Summary: Avocado Plugin for Execution of GLib Test Framework tests
Requires: python3-%{srcname} == %{version}

%description -n python3-%{srcname}-plugins-glib
This optional plugin is intended to list and run tests written in the
GLib Test Framework.

%files -n python3-%{srcname}-plugins-glib
%{python3_sitelib}/avocado_glib*
%{python3_sitelib}/avocado_framework_plugin_glib*
%config(noreplace)%{_sysconfdir}/avocado/conf.d/glib.conf

%package -n python3-%{srcname}-examples
Summary:       Avocado Test Framework Example Tests
Requires:      python3-%{srcname} == %{version}

%description -n python3-%{srcname}-examples
The set of example tests present in the upstream tree of the Avocado framework.
Some of them are used as functional tests of the framework, others serve as
examples of how to write tests on your own.

%files -n python3-%{srcname}-examples
%dir %{_docdir}/avocado
%{_docdir}/avocado/gdb-prerun-scripts
%{_docdir}/avocado/plugins
%{_docdir}/avocado/tests
%{_docdir}/avocado/wrappers
%{_docdir}/avocado/yaml_to_mux
%{_docdir}/avocado/yaml_to_mux_loader
%{_docdir}/avocado/varianter_pict
%{_docdir}/avocado/varianter_cit

%package -n python3-%{srcname}-bash
Summary:       Avocado Test Framework Bash Utilities
Requires:      python3-%{srcname} == %{version}

%description -n python3-%{srcname}-bash
A small set of utilities to interact with Avocado from the Bourne
Again Shell code (and possibly other similar shells).

%files -n python3-%{srcname}-bash
%{_libexecdir}/avocado*

%changelog
* Sat 20 Feb 2021 Zhu Huankai <zhuhuankai1@huawei.com> - 77.0-0
- Package init
